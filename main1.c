
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "alumnos.h"

int main(int argc, const char * argv[]) {
    int opcion=0;  // opcion del menu seleccionada
    int cantidad=0; // para usar en el case 1, cantidad de alumnos a crear
    int j=0; // uso general de los for
    printf("Programa de alumnos!\n");
    do {
        opcion=menu();
        //system("clear"); // clear en *NIX clr en windows
        //Aqui va la logica principal
        switch (opcion) {
            case 1:
                 printf("Crear una nueva lista de alumnos\n");
                 printf("¿Cuantos alumnos quieres crear?:");
                 scanf("%d",&cantidad);
                 for(j=0;j<cantidad;j++){
                   printf("Capturando datos del alumno %d\n",j+1);
                   nuevoAlumno();
                 }
                break;
            case 2:
                printf("Guardar lista al archivo\n");
                grabaRegistros(listaAlumnos,indiceAlArreglo);
                break;
            case 3:
                printf("Leer la lista desde el archivo\n");
                indiceAlArreglo=registrosEnArchivo();
                leerRegistros(indiceAlArreglo);
                break;
            case 4:
                printf("Mostrar todos los datos de la lista\n");
                imprimirLista();
                break;
            case 5:
                printf("Agregar un nuevo alumno a la lista\n");
                nuevoAlumno();
                break;
            case 6:
                printf("Promedio de alumnos\n");
               PromedioAlum();
                break;
            case 7:
                printf("Buscar alumno por nombre\n");
                 BuscaNombre();
                break;
            case 8:
                printf("Buscar alumno por edad\n");
                 BuscarEdad();
                break;
            case 9:
                printf("Eliminar a un alumno.\n");
                EliminarAlumno();
                grabaRegistros(listaAlumnos,indiceAlArreglo);
                break;
            case 10:
                printf("Modificar datos del alumno\n");
                EditarAlumno();
                grabaRegistros(listaAlumnos,indiceAlArreglo);
                break;
                case 11:
                printf("Buscar alumnos por genero\n");
                 BuscarGenero();
                break;
                case 12:
                printf("Uso futuro\n");
                break;
            default:
                printf("Opcion no válida\n");
                break;
        }

    } while (opcion != 0);
    return 0;
}
