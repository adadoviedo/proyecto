

#define MAX 180
#ifndef alumnos_h
#define alumnos_h
struct alumnos
{
    int edad;
    char nombre[120];
    char genero;
    char carrera[50];
    char nCuenta[10];
    float promedio;
};
typedef struct alumnos ALUMNO;

int indiceAlArreglo=0; // esta variable global se usar· para almacenar el indice del ultimo alumno ingresado al arreglo.
ALUMNO listaAlumnos[MAX]; // declaraciÛn de un arreglo de alumnos de 180 elementos


/**  Funcion insertarAlumno
 *
 * Descripci�n:
 *   Funcion para insertar al arreglo un nuevo alumno,
 * Entrada:
 *     la funci�n  recibe un alumno "al" de entrada.
 * L�gica interna:
 *     Verifica que el indice este en el rango adecuado, entre 0 y MAX.
 *     de cumplirse ingresa el alumno al arreglo e incrementa el indice general
 * Valor de retorno:
 *     No regresa nada.
 */
void insertarAlumno(ALUMNO al){
    if (indiceAlArreglo >= 0 && indiceAlArreglo < MAX) { // verificamos que inidice este en los rangos correctos
        listaAlumnos[indiceAlArreglo]=al;
        indiceAlArreglo++;
    }else{
        printf("El indice apunta fuera del arreglo, favor de revisar la l�gica");
    }
}


/** Funcion nuevoAlumno
 * Descripci�n:
 *   Funcion para capturar los datos desde teclado de un nuevo alumno,
 * Entrada:
 *     la funci�n no recibe ningun par�metro de entrada.
 * L�gica interna:
 *     Crea una variable temporal del tipo alumno y se obtienen los datos desde el teclado.
 *     Tamb�en se agrega el alumno al arreglo
 * Valor de retorno:
 *     Regreza la variable temporal de tipo ALUMNO con los datos llenos.
 */
ALUMNO nuevoAlumno(){
    ALUMNO tmp;
    printf("Introduce la edad:");
    scanf("%d",&tmp.edad);
    printf("Introduce el genero [M o F]:");
    scanf(" %c",&tmp.genero);   // El espacio antes del %c es para que ignore espacios en blanco
    printf("Introduce el nombre:");
    scanf("%*c%[^\n]",tmp.nombre);
    printf("Introduce Carrera:");
    scanf("%*c%[^\n]",tmp.carrera);
    printf("Introduce numero de cuenta:");
    scanf("%*c%[^\n]",tmp.nCuenta);
    printf("Introduce tu promedio:");
    scanf("%f",&tmp.promedio);
    /*
     * Aqui agregamos al alumno al arreglo e incrementamos el �ndice para que apunte al siguiente elementos
     */
    insertarAlumno(tmp);
    return tmp;
}
/** funcion imprimeAlumno
 * Descripci�n:
 *   Funcion para imprimir en pantalla los datos de un solo alumno,
 * Entrada:
 *     la funci�n  recibe la variable alumno a ser impresa.
 * L�gica interna:
 *     S�lo imprime cada uno de los campos
 * Valor de retorno:
 *     No regresa nada
 */
void imprimeAlumno(ALUMNO alu){
    printf("\tNombre:%s\n",alu.nombre);
    printf("\tEdad:%d\n",alu.edad);
    printf("\tGenero:%c\n",alu.genero);
    printf("\tCarrera:%s\n",alu.carrera);
    printf("\tNumero de Cuenta:%s\n",alu.nCuenta);
    printf("\tPromedio:%.2f\n",alu.promedio);
    printf("+---------------------------------+\n\n");
}

/** funcion imprimirLista
 * Descripci�n:
 *   Funcion para imprimir en pantalla TODOS los datos del arreglo.
 * Entrada:
 *     la funci�n NO recibe parametros de entrada.
 * L�gica interna:
 *     en un for recorre el arreglo hasta el indiceAlArreglo que es el que almacena el tope actual
 * Valor de retorno:
 *     No regresa nada
 */
void imprimirLista(){
    int j=0;
    for (j = 0; j < indiceAlArreglo; j++) {
        printf("+--------- # de lista: %d ---------+*\n",j+1);
        imprimeAlumno(listaAlumnos[j]);
    }
}



int menu(){
    int op=0;
    printf("\n----------- Men� para la aplicacion de BD para alumnos ---------\n");
    printf("(1) Crear lista.\n");
    printf("(2) Guardar a archivo.\n");
    printf("(3) Leer desde archivo.\n");
    printf("(4) Mostrar lista.\n");
    printf("(5) Agregar alumno. \n");
    printf("(6) Obtener promedio de alumnos.\n");
    printf("(7) Buscar alumno por nombre. \n");
    printf("(8) Buscar alumno por edad. \n");
    printf("(9) Eliminar alumno. \n");
    printf("(10) Modificar datos \n");
    printf("(11) Buscar alumnos por genero\n");
    printf("(12) Uso futuro \n");
    printf("(0) SALIR\n");
    printf("\n\nElige una opcion:");
    scanf("%d",&op);

    return op;
}



/**
Manejo de archivos
*/

/*
 Funcion para grabar un ARREGLO DE REGISTROS
 en el archivo Evaluacion.dat
 */
void grabaRegistros(ALUMNO r[], int tam){
    FILE *ptrF;

    if((ptrF=fopen("Evaluacion.dat","w"))==NULL){
        printf("el archivo no se puede abrir\n");
    }else{
        fwrite(r,sizeof(ALUMNO),tam,ptrF);
    }

    fclose(ptrF);
}

/*
 Funcion para LEER  REGISTROs
 en el archivo Evaluacion.dat
 */
void leerRegistros(int tam){

    FILE *ptrF;

    if((ptrF=fopen("Evaluacion.dat","rb"))==NULL){
        printf("el archivo no se puede abrir\n");
    }
    else{
        //for /*(int i=0;i<tam;i++)*/
        fread(listaAlumnos,sizeof(ALUMNO),tam,ptrF);
    }

    fclose(ptrF);
}

/**
 *
 * Regresa el numero de registros almacenados en el archivo
 *
 */
int registrosEnArchivo(){
    FILE *ptrF;
    int contador=0;
    ALUMNO  basura;
    if((ptrF=fopen("Evaluacion.dat","rb"))==NULL){
        printf("el archivo no se puede abrir\n");
    }
    else{
        while(!feof(ptrF)){
            if (fread(&basura,sizeof(ALUMNO),1,ptrF))
                contador++;
        }

    }
    fclose(ptrF);
    return contador;
}


float PromedioAlum(){
  float suma=0.0;
  float prom;
  ALUMNO alum;
      for (int i = 0; i < indiceAlArreglo; i++) {
        alum=listaAlumnos[i];
          suma=suma+alum.promedio;
      }
      prom=suma/indiceAlArreglo;
    printf("El promedio de los alumnos es: %.2f \n",prom);

 return prom;
}

void BuscaNombre(){
  char bnombre[120];
  ALUMNO bAlum;
printf("Ingresa el nombre que deseas buscar: " );
scanf("%*c%[^\n]",bnombre );
printf("----------------------------------------\n" );

 for (int i = 0; i < indiceAlArreglo; i++) {
   bAlum=listaAlumnos[i];
   strcmp(bAlum.nombre,bnombre);
 if (strcmp(bAlum.nombre,bnombre)==0) {
   imprimeAlumno(bAlum);
    }
  }

}
void BuscarEdad() {
  int bEdad;
  ALUMNO EdadAl;
printf("Ingresa la edad que deseas buscar: " );
scanf("%d",&bEdad );
printf("----------------------------------------\n" );
 for (int i = 0; i < indiceAlArreglo; i++) {
   EdadAl=listaAlumnos[i];
 if (EdadAl.edad==bEdad) {
   imprimeAlumno(EdadAl);
    }
  }
}

void BuscarGenero() {
  char bGenero;
  ALUMNO GenAlum;
printf("Ingresa el genero que deseas buscar: " );
scanf(" %c",&bGenero );
printf("----------------------------------------\n" );
 for (int i = 0; i < indiceAlArreglo; i++) {
   GenAlum=listaAlumnos[i];

 if (GenAlum.genero==bGenero) {
   imprimeAlumno(GenAlum);
    }
  }
}

ALUMNO EditarAlumno(){
  int a;
  int o;
  ALUMNO EditAlum;
  printf("Ingresa el numero de alumno que deseas editar: " );
   scanf("%d",&a );
   a--;
   if (a>=0 && a<indiceAlArreglo) {
     EditAlum=listaAlumnos[a];
     printf("Editar Alumno:\n" );
       imprimeAlumno(EditAlum);
     printf("-----------------------------------------\n" );
      printf("\tDatos:\n" );
        printf("(1) Edad\n" );
        printf("(2) Genero\n" );
        printf("(3) Nombre\n" );
        printf("(4) Carrera\n" );
        printf("(5) Numero de cuenta\n" );
        printf("(6) Promedio\n" );
     printf("Elige el campo a editar: \n" );
      scanf("%d",&o );
      printf("-----------------------------------------\n" );
      switch (o) {
        case 1:
          printf("La edad actual es: %d\n",EditAlum.edad );
           printf("Introduce la NUEVA edad: " );
            scanf("%d",&EditAlum.edad );
        printf("El nuevo estado del alumno %d es:\n",a );
        imprimeAlumno(EditAlum);

        break;
        case 2:
          printf("El genero actual es: %c\n",EditAlum.genero );
           printf("Introduce el NUEVO genero: " );
            scanf(" %c",&EditAlum.genero );
        printf("El nuevo estado del alumno %d es:\n",a );
        imprimeAlumno(EditAlum);

        break;
        case 3:
          printf("El nombre actual es: %s\n",EditAlum.nombre );
           printf("Introduce el NUEVO nombre: " );
            scanf("%*c%[^\n]",EditAlum.nombre );
        printf("El nuevo estado del alumno %d es:\n",a );
        imprimeAlumno(EditAlum);
        printf("IMPORTANTE: Guarda el archivo para almacenar los nuevos datos.\n" );
        break;
        case 4:
          printf("La carrera actual es: %s\n",EditAlum.carrera );
           printf("Introduce la NUEVA carrera: " );
            scanf("%*c%[^\n]",EditAlum.carrera );
        printf("El nuevo estado del alumno %d es:\n",a+1 );
        imprimeAlumno(EditAlum);

        break;
        case 5:
          printf("Numero de cuenta actual es: %s\n",EditAlum.nCuenta );
           printf("Introduce el NUEVO Numero de Cuenta: " );
            scanf("%*c%[^\n]",EditAlum.nCuenta );
        printf("El nuevo estado del alumno %d es:\n",a+1 );
        imprimeAlumno(EditAlum);

        break;
        case 6:
          printf("El promedio actual es: %f\n",EditAlum.promedio );
           printf("Introduce el NUEVO promedio: " );
            scanf("%f",&EditAlum.promedio );

        printf("El nuevo estado del alumno %d es:\n",a+1 );
        imprimeAlumno(EditAlum);
      break;
      default:
       printf("ERROR: Dato invalido.\n" );
      break;
      }
   } else {
     printf("EL alumno seleccionado no esta registrado.\n" );
   }
  listaAlumnos[a]=EditAlum;
return EditAlum;
}

ALUMNO EliminarAlumno(){
  int z;
  ALUMNO BorrarAlum;
  printf("Ingresa el numero de lista del alumno que deseas eliminar: " );
  scanf("%d",&z );
  z--;
     if (z>=0 && z<indiceAlArreglo) {
       BorrarAlum=listaAlumnos[z];
       for (int i = z; i < indiceAlArreglo; i++) {
         listaAlumnos[i]=listaAlumnos[i+1];
       }
       indiceAlArreglo--;
     } else{
       printf("ERROR: El alumno seleccionado no esta registrado.\n" );
     }
     printf("Usted elimino a:\n" );
     imprimeAlumno(BorrarAlum);
     return BorrarAlum;
}



#endif /* alumnos_h */
